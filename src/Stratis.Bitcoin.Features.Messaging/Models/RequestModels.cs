﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Stratis.Bitcoin.Features.Messaging.Models
{

    public class RequestModel
    {
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }

    public class ListenRequest : RequestModel
    {
        [Required(ErrorMessage = "The address is missing.")]
        public string Address { get; set; }
    }

    public class ConnectRequest : RequestModel
    {
        [Required(ErrorMessage = "The address is missing.")]
        public string Address { get; set; }

        [Required(ErrorMessage = "The data is missing.")]
        public string Data { get; set; }
    }

    public class ListenGeoRequest: RequestModel
    {
        [Required(ErrorMessage = "The address is missing.")]
        public string Address;

        [Required(ErrorMessage = "The latitude is missing.")]
        public double Latitude;

        [Required(ErrorMessage = "The longitude is missing.")]
        public double Longitude;

        [Required(ErrorMessage = "The radius is missing.")]
        public double Radius;
    }

    public class ConnectGeoRequest : RequestModel
    {
        [Required(ErrorMessage = "The latitude is missing.")]
        public double Latitude { get; set; }

        [Required(ErrorMessage = "The longitude is missing.")]
        public double Longitude { get; set; }

        [Required(ErrorMessage = "The radius is missing.")]
        public double Radius;

        [Required(ErrorMessage = "The data is missing.")]
        public string Data { get; set; }
    }
}
