﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using NBitcoin;
using NBitcoin.Crypto;

namespace Stratis.Bitcoin.Features.Messaging
{
    public class MessagingListener
    {
        public string Address { get; private set; }

        private ConcurrentDictionary<uint256, byte[]> _connections;
        public ConcurrentDictionary<uint256, byte[]> Connections
        {
            get
            {
                return this._connections;
            }
        }

        public MessagingListener(string address)
        {
            this.Address = address;
            this._connections = new ConcurrentDictionary<uint256, byte[]>();
        }

        public bool AddConnection(byte[] data)
        {
            return this.Connections.TryAdd(Hashes.Hash256(data), data);
        }

        public IEnumerable<byte[]> GetConnections()
        {
            var connections = new ConcurrentDictionary<uint256, byte[]>();
            connections = Interlocked.Exchange(ref this._connections, connections);
            return connections.Values;
        }
    }
}
