﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Stratis.Bitcoin.Features.Messaging.Interfaces;
using Stratis.Bitcoin.P2P.Peer;
using Stratis.Bitcoin.P2P.Protocol;
using Stratis.Bitcoin.P2P.Protocol.Behaviors;
using Stratis.Bitcoin.P2P.Protocol.Payloads;

namespace Stratis.Bitcoin.Features.Messaging
{

    public class MessagingBehavior : NetworkPeerBehavior, IMessagingBehavior
    {
        private readonly IMessaging messaging;

        /// <summary>Instance logger.</summary>
        private readonly ILogger logger;

        /// <summary>Factory for creating loggers.</summary>
        private readonly ILoggerFactory loggerFactory;
      

        public MessagingBehavior(IMessaging messaging, ILoggerFactory loggerFactory)
        {
            this.messaging = messaging;
            this.logger = loggerFactory.CreateLogger(this.GetType().FullName);
            this.loggerFactory = loggerFactory;
        }

        public override object Clone()
        {
            this.logger.LogTrace("()");

            var res = new MessagingBehavior(this.messaging, this.loggerFactory);

            this.logger.LogTrace("(-)");
            return res;
        }

        public async Task ConnectAsync(Guid id, string toAddress, byte[] data)
        {
            INetworkPeer peer = this.AttachedPeer;
            if (peer == null)
            {
                this.logger.LogTrace("(-)[NO_PEER]");
                return;
            }
    
            await peer.SendMessageAsync(new MessagingConnectionPayload(id, toAddress, data)).ConfigureAwait(false);
            this.logger.LogTrace("(-)[SEND_MESSAGE_CONNECTION_PAYLOAD]");
        }

        protected override void AttachCore()
        {
            this.logger.LogTrace("()");

            this.AttachedPeer.MessageReceived.Register(this.OnMessageReceivedAsync);

            this.logger.LogTrace("(-)");
        }

        protected override void DetachCore()
        {
            this.logger.LogTrace("()");

            this.AttachedPeer.MessageReceived.Unregister(this.OnMessageReceivedAsync);

            this.logger.LogTrace("(-)");
        }

        private async Task OnMessageReceivedAsync(INetworkPeer peer, IncomingMessage message)
        {
            this.logger.LogTrace("({0}:'{1}',{2}:'{3}')", nameof(peer), peer.RemoteSocketEndpoint, nameof(message), message.Message.Command);

            if (message.Message.Payload is MessagingConnectionPayload)
            {
                var payload = message.Message.Payload as MessagingConnectionPayload;
                await this.messaging.ConnectAsync(payload.Address, payload.Data, payload.Id, peer);
            }

            this.logger.LogTrace("(-)");
        }
    }
}
