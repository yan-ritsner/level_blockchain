﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Stratis.Bitcoin.Builder;
using Stratis.Bitcoin.Builder.Feature;
using Stratis.Bitcoin.Configuration.Logging;
using Stratis.Bitcoin.Connection;
using Stratis.Bitcoin.Features.Messaging.Controllers;
using Stratis.Bitcoin.Features.Messaging.Interfaces;
using Stratis.Bitcoin.P2P.Peer;

namespace Stratis.Bitcoin.Features.Messaging
{
    public class MessagingFeature : FullNodeFeature, IMessaging
    {
        protected readonly IConnectionManager connectionManager;

        /// <summary>Instance logger.</summary>
        private readonly ILogger logger;

        /// <summary>Factory for creating loggers.</summary>
        protected readonly ILoggerFactory loggerFactory;

        protected readonly string name;

        protected readonly ConcurrentDictionary<string, MessagingListener> listeners;

        protected readonly ConcurrentDictionary<Guid, MessagingHistory> history;

        protected readonly Timer timer;

        protected const int maxHistorySize = 50000;
        protected const int historySizeDecrease = 10000;
        protected const int maxHistoryAge = 1;
        protected const int timerDelay = 1000;
        protected const int timerPeriod = 1 * 1000 * 60;

        public MessagingFeature(
            IConnectionManager connectionManager,
            ILoggerFactory loggerFactory,
            string name = "Messaging")
        {
            this.name = name;
            this.connectionManager = connectionManager;
            this.logger = loggerFactory.CreateLogger(this.GetType().FullName);
            this.loggerFactory = loggerFactory;
            this.listeners = new ConcurrentDictionary<string, MessagingListener>();
            this.history = new ConcurrentDictionary<Guid, MessagingHistory>();
            this.timer = new Timer(TimerTick, null, timerDelay, timerPeriod);
        }

        public override void Initialize()
        {
            this.logger.LogTrace("()");

            this.connectionManager.Parameters.TemplateBehaviors.Add(new MessagingBehavior(this, this.loggerFactory));

            this.logger.LogTrace("(-)");
        }

        public override void Dispose()
        {
            this.logger.LogInformation("Stopping {0}...", this.name);
        }

        public void StartListening(string onAddress)
        {
            this.logger.LogTrace("()");

            this.listeners.TryAdd(onAddress, new MessagingListener(onAddress));

            this.logger.LogTrace($"(-)[Listening started on address: {onAddress}]");

            this.logger.LogTrace("(-)");
        }

        public void StopListening(string onAddress)
        {
            this.logger.LogTrace("()");

            MessagingListener listener;
            this.listeners.TryRemove(onAddress, out listener);

            this.logger.LogTrace($"(-)[Listening stoped on address: {onAddress}]");

            this.logger.LogTrace("(-)");
        }

        public async Task ConnectAsync(string toAddress, byte[] data, Guid? id = null, INetworkPeer sender = null)
        {
            this.logger.LogTrace("()");

            //TODO: check address and data if they exceed limits
            //TODO: check connection number exceeded
            //TODO: prevent DDOS attacks [POW]

            MessagingHistory history;

            //New message - generate id and store
            if (id == null)
            {
                if (this.history.Count >= maxHistorySize) ClearHistory();
                id = Guid.NewGuid();
                history = new MessagingHistory(id.Value, DateTime.UtcNow);
                this.history.TryAdd(id.Value, history);
            }
            //Сheck if message is already seen
            else if (this.history.TryGetValue(id.Value, out history))
            {
                return;
            }

            //If address matches - add connection to local listener
            MessagingListener listener;
            if (this.listeners.TryGetValue(toAddress, out listener))
            {
                if (listener.AddConnection(data))
                {
                    this.logger.LogTrace($"(-)[Added connection to address: {toAddress}]");
                }
            }
            //If not - relay connection request to peers exclude sender
            else
            {
                var peers = this.connectionManager.ConnectedPeers.Where(s => s != sender);
                if (!peers.Any())
                {
                    this.logger.LogTrace("(-)[NO_PEERS]");
                    return;
                }
                //Send connect request
                var behaviours = peers.Select(s => s.Behavior<MessagingBehavior>());

                foreach (var behaviour in behaviours)
                {
                    try
                    {
                        await behaviour.ConnectAsync(id.Value, toAddress, data).ConfigureAwait(false);
                    }
                    catch (Exception ex)
                    {
                        this.logger.LogTrace($"(-)[ERROR] error occurred while sending connection request to one of the peers: {ex}]");
                    }
                }

                this.logger.LogTrace($"(-)[Relayed connection to address: {toAddress}]");
            }

            this.logger.LogTrace("(-)");
        }

        public IEnumerable<byte[]> GetConnections(string toAddress)
        {
            this.logger.LogTrace("()");

            MessagingListener listener;
            if (this.listeners.TryGetValue(toAddress, out listener))
            {
                this.logger.LogTrace($"(-)[Getting connections for address: {toAddress}]");
                return listener.GetConnections();
            }
      
            this.logger.LogTrace("(-)");

            return null;
        }

        public void ClearHistory()
        {
            this.logger.LogTrace("()");

            List<Guid> remove = new List<Guid>();
            MessagingHistory h;

            foreach (var history in this.history)
            {
                if(DateTime.UtcNow - history.Value.Timestamp > TimeSpan.FromMinutes(maxHistoryAge))
                {
                    remove.Add(history.Key);
                }
            }
    
            foreach(var r in remove)
            {
                this.history.TryRemove(r, out h);
            }

            if(this.history.Count >= maxHistorySize)
            {
                remove.Clear();
      
                foreach(var history in this.history.OrderBy(kvp=>kvp.Value.Timestamp))
                {
                    if (remove.Count >= historySizeDecrease) break;
                    remove.Add(history.Key);
                }

                foreach (var r in remove)
                {
                    this.history.TryRemove(r, out h);
                }
            }

            this.logger.LogTrace("(-)");
        }


        private void TimerTick(object state)
        {
            ClearHistory();
        }

    }

    /// <summary>
    /// A class providing extension methods for <see cref="IFullNodeBuilder"/>.
    /// </summary>
    public static class FullNodeBuilderBlockStoreExtension
    {
        public static IFullNodeBuilder UseMessaging(this IFullNodeBuilder fullNodeBuilder)
        {
            LoggingConfiguration.RegisterFeatureNamespace<MessagingFeature>("messaging");

            fullNodeBuilder.ConfigureFeature(features =>
            {
                features
                .AddFeature<MessagingFeature>()
                .FeatureServices(services =>
                {
                    services.AddSingleton<MessagingController>();
                });
            });

            return fullNodeBuilder;
        }
    }
}
