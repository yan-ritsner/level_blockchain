﻿using System;

namespace Stratis.Bitcoin.Features.Messaging
{
    public class MessagingHistory
    {
        public readonly Guid Id;

        public readonly DateTime Timestamp;

        public MessagingHistory(Guid id,DateTime timestamp)
        {
            this.Id = id;
            this.Timestamp = timestamp;
        }
    }
}
