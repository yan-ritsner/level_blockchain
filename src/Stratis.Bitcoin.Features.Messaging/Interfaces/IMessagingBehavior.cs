﻿using System;
using System.Threading.Tasks;
using Stratis.Bitcoin.P2P.Protocol.Behaviors;

namespace Stratis.Bitcoin.Features.Messaging.Interfaces
{
    public interface IMessagingBehavior : INetworkPeerBehavior
    {
         Task ConnectAsync(Guid id, string toAddress, byte[] data);
    }
}
