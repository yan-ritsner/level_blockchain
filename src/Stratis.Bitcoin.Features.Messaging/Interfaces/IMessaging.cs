﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Stratis.Bitcoin.P2P.Peer;

namespace Stratis.Bitcoin.Features.Messaging.Interfaces
{
    public interface IMessaging
    {
        void StartListening(string onAddress);

        void StopListening(string onAddress);

        Task ConnectAsync(string toAddress, byte[] data, Guid? id, INetworkPeer sender);

        IEnumerable<byte[]> GetConnections(string toAddress);

        void ClearHistory();
    }
}
