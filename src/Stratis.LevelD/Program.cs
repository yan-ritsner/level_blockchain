using System;
using System.Linq;
using System.Threading.Tasks;
using NBitcoin;
using NBitcoin.Crypto;
using NBitcoin.Protocol;
using Stratis.Bitcoin.Builder;
using Stratis.Bitcoin.Configuration;
using Stratis.Bitcoin.Features.Api;
using Stratis.Bitcoin.Features.BlockStore;
using Stratis.Bitcoin.Features.Consensus;
using Stratis.Bitcoin.Features.MemoryPool;
using Stratis.Bitcoin.Features.Messaging;
using Stratis.Bitcoin.Features.Miner;
using Stratis.Bitcoin.Features.RPC;
using Stratis.Bitcoin.Features.Wallet;
using Stratis.Bitcoin.Utilities;

namespace Stratis.LevelD
{
    public class Program
    {
        public static void Main(string[] args)
        {
            MainAsync(args).Wait();
            //GenerateGenesis();
            
        }

        public static void GenerateMainGenesis()
        {
            Block.BlockSignature = true;
            Transaction.TimeStamp = true;

            var consensus = new Consensus
            {
                NetworkOptions = new NetworkOptions() { IsProofOfStake = true },
                GetPoWHash = (n, h) => HashX13.Instance.Hash(h.ToBytes(options: n)),
                PowLimit = new Target(new uint256("00000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"))
            };

            //Change timestamp to regenerate
            Block genesis = Network.CreateLevelGenesisBlock(1519321587, 0, 0x1e0fffff, 1, Money.Zero);

            while (!genesis.CheckProofOfWork(consensus))
            {
                ++genesis.Header.Nonce;
            }

            var nonce = genesis.Header.Nonce;
            var hash = genesis.GetHash(consensus.NetworkOptions);
            var root = genesis.Header.HashMerkleRoot;

            Console.ReadLine();
        }

        public static void GenerateTestGenesis()
        {
            Block.BlockSignature = true;
            Transaction.TimeStamp = true;

            var consensus = Network.LevelMain.Consensus.Clone();
            consensus.PowLimit = new Target(uint256.Parse("0000ffff00000000000000000000000000000000000000000000000000000000"));

            var genesis = Network.LevelMain.GetGenesis();
            genesis.Header.Time = 1519406174; //Change timestamp to regenerate
            genesis.Header.Nonce = 0;
            genesis.Header.Bits = consensus.PowLimit;

            while (!genesis.CheckProofOfWork(consensus))
            {
                ++genesis.Header.Nonce;
            }

            var nonce = genesis.Header.Nonce;
            var hash = genesis.GetHash(consensus.NetworkOptions);
            var root = genesis.Header.HashMerkleRoot;

            Console.ReadLine();
        }

        public static void GenerateRegGenesis()
        {
            Block.BlockSignature = true;
            Transaction.TimeStamp = true;

            var consensus = Network.LevelTest.Consensus.Clone();
            consensus.PowLimit = new Target(uint256.Parse("7fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"));

            consensus.PowAllowMinDifficultyBlocks = true;
            consensus.PowNoRetargeting = true;

            var genesis = Network.LevelMain.GetGenesis();
            genesis.Header.Time = 1519409489; //Change timestamp to regenerate
            genesis.Header.Nonce = 0;
            genesis.Header.Bits = consensus.PowLimit;

            while (!genesis.CheckProofOfWork(consensus))
            {
                ++genesis.Header.Nonce;
            }

            var nonce = genesis.Header.Nonce;
            var hash = genesis.GetHash(consensus.NetworkOptions);
            var root = genesis.Header.HashMerkleRoot;

            Console.ReadLine();
        }

        public static async Task MainAsync(string[] args)
        {
            try
            {
                Network network = args.Contains("-testnet") ? Network.LevelTest : Network.LevelMain;
                NodeSettings nodeSettings = new NodeSettings(network, ProtocolVersion.ALT_PROTOCOL_VERSION, args:args, loadConfiguration:false);

                var node = new FullNodeBuilder()
                    .UseNodeSettings(nodeSettings)
                    .UsePosConsensus()
                    .UseBlockStore()
                    .UseMempool()
                    .UseWallet()
                    .AddPowPosMining()
                    .UseMessaging()
                    .UseApi()
                    .AddRPC()
                    .Build();

                await node.RunAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was a problem initializing the node. Details: '{0}'", ex.Message);
            }
        }
    }
}
