﻿using System;
using NBitcoin;
using NBitcoin.DataEncoders;
using NBitcoin.Protocol;

namespace Stratis.Bitcoin.P2P.Protocol.Payloads
{
    [Payload("msgcnn")]
    public class MessagingConnectionPayload : Payload, IBitcoinSerializable
    {
        private VarString id = new VarString();
        public Guid Id
        {
            get
            {
                return new Guid(this.id.GetString());
            }
            set
            {
                this.id = new VarString(value.ToByteArray());
            }
        }

        private VarString address = new VarString();
        public string Address
        {
            get
            {
                return Encoders.ASCII.EncodeData(this.address.GetString(true));
            }
            set
            {
                this.address = new VarString(Encoders.ASCII.DecodeData(value));
            }
        }

        private VarString data = new VarString();
        public byte[] Data
        {
            get
            {
                return this.data.GetString();
            }
            set
            {
                this.data = new VarString(value);
            }
        }

        public MessagingConnectionPayload()
        {

        }


        public MessagingConnectionPayload(Guid id, string address, byte[] data)
        {
            this.Id = id;
            this.Address = address;
            this.Data = data;
        }

        public override void ReadWriteCore(BitcoinStream stream)
        {
            stream.ReadWrite(ref this.id);
            stream.ReadWrite(ref this.address);
            stream.ReadWrite(ref this.data);
        }

        public override string ToString()
        {
            return this.Address + " message connection";
        }
    }

    [Payload("msgcnngeo")]
    public class MessagingConnectionGeoPayload : Payload, IBitcoinSerializable
    {
        private VarString id = new VarString();
        public Guid Id
        {
            get
            {
                return new Guid(this.id.GetString());
            }
            set
            {
                this.id = new VarString(value.ToByteArray());
            }
        }

        private double latitude;
        public double Latitude
        {
            get
            {
                return this.latitude;
            }
            set
            {
                this.latitude = value;
            }
        }

        private double longitude;
        public double Longitude
        {
            get
            {
                return this.longitude;
            }
            set
            {
                this.longitude = value;
            }
        }

        private double radius;
        public double Radius
        {
            get
            {
                return this.radius;
            }
            set
            {
                this.radius = value;
            }
        }

        private VarString data = new VarString();


        public byte[] Data
        {
            get
            {
                return this.data.GetString();
            }
            set
            {
                this.data = new VarString(value);
            }
        }

        public MessagingConnectionGeoPayload()
        {

        }

        public MessagingConnectionGeoPayload(Guid id, double latitude, double longitude, double radius, byte[] data)
        {
            this.Id = id;
            this.Latitude = latitude;
            this.Longitude = longitude;
            this.Radius = radius;
            this.Data = data;
        }

        public override void ReadWriteCore(BitcoinStream stream)
        {
            stream.ReadWrite(ref this.id);
            stream.ReadWrite(ref this.latitude);
            stream.ReadWrite(ref this.longitude);
            stream.ReadWrite(ref this.radius);
            stream.ReadWrite(ref this.data);
        }

        public override string ToString()
        {
            return "Lat: " + this.Latitude + "Lon: " + this.Longitude + "Rad: " + this.Radius + " geo message connection";
        }
    }
}
